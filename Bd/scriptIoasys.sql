USE [master]
GO
/****** Object:  Database [ProjetoEmpresa]    Script Date: 18/04/2019 02:00:10 ******/
CREATE DATABASE [ProjetoEmpresa]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProjetoEmpresa', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\ProjetoEmpresa.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ProjetoEmpresa_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\ProjetoEmpresa_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ProjetoEmpresa] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProjetoEmpresa].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProjetoEmpresa] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProjetoEmpresa] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProjetoEmpresa] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET  ENABLE_BROKER 
GO
ALTER DATABASE [ProjetoEmpresa] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProjetoEmpresa] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [ProjetoEmpresa] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET RECOVERY FULL 
GO
ALTER DATABASE [ProjetoEmpresa] SET  MULTI_USER 
GO
ALTER DATABASE [ProjetoEmpresa] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProjetoEmpresa] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProjetoEmpresa] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProjetoEmpresa] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProjetoEmpresa] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ProjetoEmpresa', N'ON'
GO
ALTER DATABASE [ProjetoEmpresa] SET QUERY_STORE = OFF
GO
USE [ProjetoEmpresa]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [ProjetoEmpresa]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 18/04/2019 02:00:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empresa]    Script Date: 18/04/2019 02:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empresa](
	[Id] [uniqueidentifier] NOT NULL,
	[Ativo] [bit] NOT NULL,
	[DataCadastro] [datetime2](7) NOT NULL,
	[Descricao] [varchar](50) NOT NULL,
	[IdTipo] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipo]    Script Date: 18/04/2019 02:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipo](
	[Id] [uniqueidentifier] NOT NULL,
	[Ativo] [bit] NOT NULL,
	[DataCadastro] [datetime2](7) NOT NULL,
	[Nome] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Tipo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 18/04/2019 02:00:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id] [uniqueidentifier] NOT NULL,
	[Ativo] [bit] NOT NULL,
	[DataCadastro] [datetime2](7) NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[SobreNome] [varchar](100) NOT NULL,
	[Senha] [varchar](32) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[EmailConfirmed] [bit] NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[TwoFactorEnabled] [bit] NULL,
	[LockoutEndDateUtc] [datetime2](7) NULL,
	[LockoutEnabled] [bit] NULL,
	[AccessFailedCount] [int] NULL,
	[Foto] [nvarchar](max) NULL,
	[DtLog] [datetime2](7) NULL,
	[Login] [varchar](50) NULL,
	[QtdeBloqueios] [int] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190418024834_v1', N'2.1.8-servicing-32085')
INSERT [dbo].[Empresa] ([Id], [Ativo], [DataCadastro], [Descricao], [IdTipo]) VALUES (N'f9daed1f-fea4-4f6d-9168-0baedf07f7fc', 1, CAST(N'2019-04-17T23:57:40.6600000' AS DateTime2), N'IBM', N'a1b49a35-740a-4198-9b1a-8ab48187b58e')
INSERT [dbo].[Empresa] ([Id], [Ativo], [DataCadastro], [Descricao], [IdTipo]) VALUES (N'4b10b224-8e61-426e-8ef5-7622b08b54c6', 1, CAST(N'2019-04-17T23:56:36.7700000' AS DateTime2), N'Microsoft', N'a1b49a35-740a-4198-9b1a-8ab48187b58e')
INSERT [dbo].[Tipo] ([Id], [Ativo], [DataCadastro], [Nome]) VALUES (N'a1b49a35-740a-4198-9b1a-8ab48187b58e', 1, CAST(N'2019-04-17T23:55:24.0800000' AS DateTime2), N'Fisica')
INSERT [dbo].[Tipo] ([Id], [Ativo], [DataCadastro], [Nome]) VALUES (N'ba4fc480-249d-44b1-b73f-bef4367d2592', 1, CAST(N'2019-04-17T23:55:26.8900000' AS DateTime2), N'Juridica')
INSERT [dbo].[Usuario] ([Id], [Ativo], [DataCadastro], [Nome], [SobreNome], [Senha], [Email], [EmailConfirmed], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [Foto], [DtLog], [Login], [QtdeBloqueios]) VALUES (N'0859c705-9a1f-4296-aac0-1ef31936afe2', 1, CAST(N'2019-03-17T15:19:43.7235286' AS DateTime2), N'Moacir', N'Emidio dos Santos Junior', N'286c2b1dd5d749e6dbdd4ae896b25788', N'moaemidiojr2018@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2019-03-17T15:19:43.7235587' AS DateTime2), NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Ativo], [DataCadastro], [Nome], [SobreNome], [Senha], [Email], [EmailConfirmed], [SecurityStamp], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [Foto], [DtLog], [Login], [QtdeBloqueios]) VALUES (N'b9c9c40e-dfb6-46dd-8b51-c66159e14a62', 1, CAST(N'2019-04-18T00:32:22.6191582' AS DateTime2), N'ioasys', N'ioasys', N'0c762485f23575522952c6641c0e5da8', N'testeapple@ioasys.com.br', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2019-04-18T00:32:22.6189689' AS DateTime2), NULL, NULL)
/****** Object:  Index [IX_Empresa_IdTipo]    Script Date: 18/04/2019 02:00:13 ******/
CREATE NONCLUSTERED INDEX [IX_Empresa_IdTipo] ON [dbo].[Empresa]
(
	[IdTipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Empresa]  WITH CHECK ADD  CONSTRAINT [FK_Empresa_Tipo_IdTipo] FOREIGN KEY([IdTipo])
REFERENCES [dbo].[Tipo] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Empresa] CHECK CONSTRAINT [FK_Empresa_Tipo_IdTipo]
GO
USE [master]
GO
ALTER DATABASE [ProjetoEmpresa] SET  READ_WRITE 
GO
