﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Map
{
    public class EmpresaMap : IEntityTypeConfiguration<Empresa>
    {
        public void Configure(EntityTypeBuilder<Empresa> builder)
        {
            // Key
            builder.HasKey(x => x.Id);

            //colunas
            builder.ToTable("Empresa");
            builder.Property(t => t.Descricao).HasColumnName("Descricao").HasColumnType("varchar(50)").IsRequired();
            builder.Property(t => t.Ativo).HasColumnName("Ativo").IsRequired();
            builder.Property(t => t.DataCadastro).HasColumnName("DataCadastro").IsRequired();

            builder.HasOne(x => x.Tipo).WithMany().HasForeignKey("IdTipo");
        }
    }
}
