﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain.Entities;

namespace Infra.Data.Map
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            // Primary Key
            builder.HasKey(t => t.Id);
            
            // Table & Column Mappings
            builder.ToTable("Usuario");
            builder.Property(t => t.Nome).HasColumnName("Nome").HasColumnType("varchar(50)").IsRequired();
            builder.Property(t => t.SobreNome).HasColumnName("SobreNome").HasColumnType("varchar(100)").IsRequired();
            builder.Property(t => t.Email).HasColumnName("Email").HasColumnType("varchar(250)").IsRequired();
            builder.Property(t => t.EmailConfirmed).HasColumnName("EmailConfirmed");
            builder.Property(t => t.Senha).HasColumnName("Senha").IsRequired().HasColumnType("varchar(32)");
            builder.Property(t => t.SecurityStamp).HasColumnName("SecurityStamp");
            builder.Property(t => t.TwoFactorEnabled).HasColumnName("TwoFactorEnabled");
            builder.Property(t => t.LockoutEndDateUtc).HasColumnName("LockoutEndDateUtc");
            builder.Property(t => t.LockoutEnabled).HasColumnName("LockoutEnabled");
            builder.Property(t => t.AccessFailedCount).HasColumnName("AccessFailedCount");
            builder.Property(t => t.Foto).HasColumnName("Foto").HasColumnType("nvarchar(max)");
            builder.Property(t => t.DtLog).HasColumnName("DtLog");
            builder.Property(t => t.Ativo).HasColumnName("Ativo");
            builder.Property(t => t.Login).HasColumnName("Login").HasColumnType("varchar(50)"); 
            builder.Property(t => t.QtdeBloqueios).HasColumnName("QtdeBloqueios");

        }
    }
}