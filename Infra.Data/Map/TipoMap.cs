﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infra.Data.Map
{
    public class TipoMap : IEntityTypeConfiguration<Tipo>
    {
        public void Configure(EntityTypeBuilder<Tipo> builder)
        {
            // Key
            builder.HasKey(x => x.Id);

            //colunas
            builder.ToTable("Tipo");
            builder.Property(t => t.Nome).HasColumnName("Nome").HasColumnType("varchar(100)").IsRequired();
            builder.Property(t => t.Ativo).HasColumnName("Ativo").IsRequired();
            builder.Property(t => t.DataCadastro).HasColumnName("DataCadastro").IsRequired();
        }
    }
}
