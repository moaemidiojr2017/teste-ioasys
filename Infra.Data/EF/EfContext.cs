﻿using Microsoft.EntityFrameworkCore;
using prmToolkit.NotificationPattern;
using Domain.Entities;
using Infra.Data.Map;
using Domain.ViewModel;

namespace Infra.Data.EF
{
    public class EfContext : DbContext
    {
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Tipo> Tipo { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Settings.ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // ignorar classes
            modelBuilder.Ignore<Notification>();
            modelBuilder.Entity<Usuario>();

            // Fazendo o mapeamentos das entidades
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new EmpresaMap());
            modelBuilder.ApplyConfiguration(new TipoMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}