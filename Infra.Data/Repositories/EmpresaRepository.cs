﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using Infra.Data.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using static Domain.ViewModel.EmpresaViewModel;

namespace Infra.Data.Repositories
{
    public class EmpresaRepository : RepositoryBase<Empresa>, IEmpresaRepository
    {
        private readonly EfContext _context;

        public EmpresaRepository(EfContext context): base(context)
        {
            _context = context;
        }

        public List<Empresa> SearchFilterAsync(Guid id, string descrica)
        {
            var query = _context.Empresa.Include(x => x.Tipo).ToList();

            if (id != null)
            {
                query.Where(x => x.Id == id || x.Id == null);
            }

            if (!string.IsNullOrEmpty(descrica))
            {
                query.Where(x => x.Descricao == descrica || x.Descricao == "NULL");
            }

            return query.ToList();
        }
    }
}
