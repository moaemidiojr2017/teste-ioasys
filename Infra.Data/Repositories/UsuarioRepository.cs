﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Infra.Data.EF;
using System.Linq;

namespace Infra.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        private readonly EfContext _context;
        
        public UsuarioRepository(EfContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Usuario> ExisteEmail(string email)
        {
            return await _context.Usuario.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<Usuario> AutenticarUsuario(string email, string senha)
        {
            return await _context.Usuario.FirstOrDefaultAsync(x => x.Email == email && x.Senha == senha);
        }
        
        public async Task<List<Usuario>> RetornarTodosUsuarioOrdenadosNomeAsync()
        {
            return await _context.Usuario.OrderBy(x=>x.Nome).AsNoTracking().ToListAsync();
        }
    }
}