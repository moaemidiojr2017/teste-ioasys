﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using Infra.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Data.Repositories
{
    public class TipoRepository : RepositoryBase<Tipo>, ITipoRepository
    {
        public TipoRepository(EfContext context) : base(context)
        {
        }
    }
}
