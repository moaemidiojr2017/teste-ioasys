﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Interfaces.Repositories;
using Infra.Data.EF;

namespace Infra.Data.Repositories
{
    public class RepositoryBase<T>: IRepositoryBase<T> where T: class
    {
        private readonly EfContext _context;

        public RepositoryBase(EfContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _context.Set<T>().AsNoTracking().ToListAsync();
        }

        public async Task<T> GetId(int id)
        {
            return await _context.FindAsync<T>(id);
        }

        public async Task<T> GetId(Guid id)
        {
            return await _context.FindAsync<T>(id);
        }

        public async Task<T> GetId(string id)
        {
            return await _context.FindAsync<T>(id);
        }

        public async Task<T> AddAsync(T model)
        {
            await _context.Set<T>().AddAsync(model);
            await _context.SaveChangesAsync();
            return model;
        }

        public async Task<T> UpdateAsync(T model)
        {
            _context.Entry(model).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return  model;
        }

        public async Task<T> DeleteAsync(T model)
        {
             _context.Set<T>().Remove(model);
            await _context.SaveChangesAsync();
            return  model;
        }
    }
}