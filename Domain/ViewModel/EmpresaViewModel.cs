﻿using Domain.Entities;
using prmToolkit.NotificationPattern;
using System;

namespace Domain.ViewModel
{
    public class EmpresaViewModel : Notifiable
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public DateTime DataCadastro { get; set; }
        public bool Ativo { get; set; }

        public void Validate()
        {
            new AddNotifications<EmpresaViewModel>(this)
                .IfNullOrInvalidLength(x => x.Descricao, 2, 50, "Descricao deve ser preenchido!");
        }

        public static explicit operator EmpresaViewModel(Empresa model)
        {
            return new EmpresaViewModel
            {
                Descricao = model.Descricao,
                Id = model.Id,               
                Ativo = model.Ativo,
                DataCadastro = model.DataCadastro
            };
        }

        public class EmpresaFiltro
        {
            public Guid? IdEmpresa { get; set; }          
            public string Nome { get; set; }
        }
    }
}
