﻿using System;
using prmToolkit.NotificationPattern;
using Domain.Extensions;

namespace Domain.ViewModel
{
    public class LoginViewModel : Notifiable
    {
        public string Email { get; set; }
        public string Senha { get; set; }
        public Guid Empresa { get; set; }

        public void Validate()
        {
            new AddNotifications<LoginViewModel>(this)
                .IfNotEmail(x => x.Email, "E-mail inválido")
                .IfNullOrInvalidLength(x => x.Senha, 6, 32, "Senha deve ser preenchido!");

            Senha = Senha.ConvertMd5();
        }
    }
}