﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using prmToolkit.NotificationPattern;

namespace Domain.ViewModel
{
    public class UsuarioViewModel : Notifiable
    {
        public Guid Id { get; set; }
        public string NomeCompleto { get; set; }
        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public string Senha { get; set; }
        public string ConfirmarSenha { get; set; }
        public string Email { get; set; }
        public bool? EmailConfirmed { get; set; }
        public string SecurityStamp { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool? TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string Foto { get; set; }
        public DateTime DtLog { get; set; }
        public bool Ativo { get; set; }
        public string Login { get; set; }
        public int QtdeBloqueios { get; set; }

        public void Validate()
        {
            new AddNotifications<UsuarioViewModel>(this)
                .IfNullOrInvalidLength(x => x.Nome, 3, 50, "Nome deve ser preenchido!")
                .IfNullOrInvalidLength(x => x.SobreNome, 3, 100, "SobreNome deve ser preenchido!")
                .IfNotEmail(x => x.Email, "E-mail inválido")
                .IfNullOrInvalidLength(x => x.Senha, 6, 32, "A Senha deve conter até 6 a 32 caracteres")
                .IfNullOrInvalidLength(x => x.ConfirmarSenha, 6, 32, "A Confirmação deve conter até 6 a 32 caracteres")
                .IfNotAreEquals(Senha, ConfirmarSenha, "Senhas", "As senhas não coincidem");
        }

        public static explicit operator UsuarioViewModel(Usuario usuario)
        {
            return  new UsuarioViewModel
            {
                Id = usuario.Id,
                NomeCompleto = usuario.Nome + " " + usuario.SobreNome,
                Nome = usuario.Nome,
                SobreNome = usuario.SobreNome,
                Email = usuario.Email,
                Foto = usuario.Foto,
                Ativo = usuario.Ativo,
                Login = usuario.Login
            };
        }
    }
}