﻿using System;

namespace Domain.Extensions
{
    public static class StringExtension
    {
        public static string ConvertMd5(this string text)
        {
            if (string.IsNullOrEmpty(text)) return "";

            var password = (text += "|2d331cca-f6c0-40c0-bb43-6e32989c1987");
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] data = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(password));
            System.Text.StringBuilder sbString = new System.Text.StringBuilder();
            for (int i = 0; i < data.Length; i++)
                sbString.Append(data[i].ToString("x2"));

            return sbString.ToString();
        }

        public static string LimparCaracteres(this string texto)
        {
            if (!string.IsNullOrEmpty(texto))
                return texto.Replace("-", "").Replace(".", "");

            return null;
        }
    }
}