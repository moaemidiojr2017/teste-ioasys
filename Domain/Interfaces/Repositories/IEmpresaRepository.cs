﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using static Domain.ViewModel.EmpresaViewModel;

namespace Domain.Interfaces.Repositories
{
    public interface IEmpresaRepository : IRepositoryBase<Empresa>
    {
        List<Empresa> SearchFilterAsync(Guid id, string descrica);
    }
}
