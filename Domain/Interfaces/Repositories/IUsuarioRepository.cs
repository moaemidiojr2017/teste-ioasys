﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;

namespace Domain.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>
    {
        Task<Usuario> ExisteEmail(string email);
        Task<Usuario> AutenticarUsuario(string email, string senha);

        Task<List<Usuario>> RetornarTodosUsuarioOrdenadosNomeAsync();
    }
}