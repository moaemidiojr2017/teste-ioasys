﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetId(int id);
        Task<T> GetId(Guid id);
        Task<T> GetId(string id);
        Task<T> AddAsync(T model);
        Task<T> UpdateAsync(T model);
        Task<T> DeleteAsync(T model);
    }
}