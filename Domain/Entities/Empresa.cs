﻿using Domain.Entities.Base;
using Domain.ViewModel;
using prmToolkit.NotificationPattern;
using System;
using System.Collections;

namespace Domain.Entities
{
    public class Empresa : Entity
    {
        protected Empresa() {  }

        public Empresa(string descricao)
        {
            Descricao = descricao;
            DataCadastro = DateTime.Now;
            Ativo = true;
        }

        public void Validate()
        {
            new AddNotifications<Empresa>(this)
                .IfNullOrInvalidLength(x => x.Descricao, 2, 50, "Descricao deve ser preenchido!");
        }

        public static explicit operator Empresa(EmpresaViewModel model)
        {
            return new Empresa
            {
                Id = model.Id,
                Descricao = model.Descricao,
                Ativo = model.Ativo,
                DataCadastro = model.DataCadastro
            };
        }

        public string Descricao { get; private set; }

        public Guid IdTipo { get; set; }
        public Tipo Tipo { get; set; }
    }
}
