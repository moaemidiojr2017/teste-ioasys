﻿using Domain.Entities.Base;

namespace Domain.Entities
{
    public class Tipo :  Entity
    {
        public string Nome { get; set; }
    }
}
