﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using prmToolkit.NotificationPattern;
using Domain.Entities.Base;
using Domain.ViewModel;
using Domain.Extensions;

namespace Domain.Entities
{
    public class Usuario : Entity
    {
        protected Usuario() { }

        public Usuario(string nome, string sobrenome, string senha, string confirmarSenha, string email)
        {
            Nome = nome;
            SobreNome = sobrenome;
            Senha = !string.IsNullOrEmpty(senha) ? senha.ConvertMd5() : null;
            ConfirmarSenha = !string.IsNullOrEmpty(senha) ? confirmarSenha.ConvertMd5() : null;
            Email = email;          
            DtLog = DateTime.Now;
            DataCadastro = DateTime.Now;
            Ativo = true;
        }

        public bool Authenticate(string email, string senha)
        {
            if (Email == email && Senha == senha.ConvertMd5())
                return true;

            new AddNotifications<Usuario>(this)
                .IfAreEquals(Senha, ConfirmarSenha, "Usuário ou senha inválidos!");

            return false;
        }

        public static explicit operator Usuario(UsuarioViewModel model)
        {
            return new Usuario
            {
                Id = model.Id,
                Nome = model.Nome,
                SobreNome = model.SobreNome,
                Senha = model.Senha,
                ConfirmarSenha = model.ConfirmarSenha,
                Email = model.Email,
                Ativo = model.Ativo,
            };
        }

        public string Nome { get; private set; }
        public string SobreNome { get; private set; }
        public string Senha { get; private set; }
        [NotMapped]
        public string ConfirmarSenha { get; private set; }
        public string Email { get; private set; }
        public bool? EmailConfirmed { get; private set; }
        public string SecurityStamp { get; private set; }      
        public bool? TwoFactorEnabled { get; private set; }
        public DateTime? LockoutEndDateUtc { get; private set; }
        public bool? LockoutEnabled { get; private set; }
        public int? AccessFailedCount { get; private set; }
        public string Foto { get; private set; }
        public DateTime? DtLog { get; private set; }
        public string Login { get; private set; }
        public int? QtdeBloqueios { get; private set; }

        public void Validate()
        {
            new AddNotifications<Usuario>(this)
                .IfNullOrInvalidLength(x => x.Nome, 3, 50, "Nome deve ser preenchido!")
                .IfNullOrInvalidLength(x => x.SobreNome, 3, 100, "SobreNome deve ser preenchido!")
                .IfNotEmail(x => x.Email, "E-mail inválido")              
                .IfNullOrInvalidLength(x => x.Senha, 6, 32, "A Senha deve conter até 6 a 32 caracteres")
                .IfNullOrInvalidLength(x => x.ConfirmarSenha, 6, 32, "A Confirmação deve conter até 6 a 32 caracteres")
                .IfNotAreEquals(Senha, ConfirmarSenha, "Senhas", "As senhas não coincidem");
        }
    }
}