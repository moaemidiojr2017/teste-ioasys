﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using prmToolkit.NotificationPattern;

namespace Domain.Entities.Base
{
    public abstract class Entity : Notifiable
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual Guid Id { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}