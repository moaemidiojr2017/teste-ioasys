﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using projetoempresas.Api.Security;
using projetoempresas.Api.ViewModel;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.ViewModel;


namespace projetoempresas.Api.Controllers
{
    public class AccountController : Controller
    {

        private readonly IUsuarioRepository _usuarioRepository;

        public AccountController(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }


        [AllowAnonymous]
        [HttpPost]
        [Route("v1/account/autenticar")]
        public async Task<ResultViewModel> Autenticar([FromBody]LoginViewModel request, [FromServices]SigningConfigurations signingConfigurations, [FromServices]TokenJwtOptions tokenConfigurations)
        {
            bool credenciaisValidas = false;

            request.Validate();

            if (request.Notifications.Count > 0)
                return new ResultViewModel(false, "Error ao fazer a autenticação", request.Notifications);

            var response = await _usuarioRepository.AutenticarUsuario(request.Email, request.Senha);

            credenciaisValidas = response != null;

            if (credenciaisValidas)
            {
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(response.Id.ToString(), "Id"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        //new Claim(JwtRegisteredClaimNames.UniqueName, response.Usuario)
                        new Claim("Usuario", JsonConvert.SerializeObject(response))
                    }
                );

                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao + TimeSpan.FromSeconds(tokenConfigurations.Seconds);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = tokenConfigurations.Issuer,
                    Audience = tokenConfigurations.Audience,
                    SigningCredentials = signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });

                var token = handler.WriteToken(securityToken);


                return new ResultViewModel
                {
                    Success = true,
                    Message = "ok",
                    Data = new DadosAutenticacao
                    {
                        Authenticated = true,
                        Created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                        Expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                        AccessToken = token,
                        Message = "ok",
                        Usuario = response
                    }
                };
            }

            return new ResultViewModel(false, "Error ao fazer a autenticação", request.Notifications);
        }
    }
    
}
