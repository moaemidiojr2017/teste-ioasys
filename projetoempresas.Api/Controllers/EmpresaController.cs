﻿using System;
using System.Threading.Tasks;
using projetoempresas.Api.ViewModel;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static Domain.ViewModel.EmpresaViewModel;

namespace projetoempresas.Api.Controllers
{
    [AllowAnonymous]
    public class EmpresaController : Controller
    {
        private readonly IEmpresaRepository _empresaRepository;

        public EmpresaController(IEmpresaRepository empresaRepository)
        {
            _empresaRepository = empresaRepository;
        }

        [HttpGet]
        [Route("v1/empresa")]
        public async Task<ResultViewModel> GetAll()
        {
            try
            {
                var response = await _empresaRepository.GetAll();
                return new ResultViewModel(true, "ok", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao selecionar a Empresa!", ex.Message);
            }
        }

        [HttpGet]
        [Route("v1/SearchFilter/")]
        public async Task<ResultViewModel> SearchFilter(Guid id, string descricao)
        {
            try
            {
                var response = _empresaRepository.SearchFilterAsync(id, descricao);
                return new ResultViewModel(true, "ok", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao selecionar a Empresa!", ex.Message);
            }
        }

        [HttpGet]
        [Route("v1/empresa/{id}")]
        public async Task<ResultViewModel> GetById(Guid id)
        {
            try
            {
                var response = await _empresaRepository.GetId(id);
                return new ResultViewModel(true, "ok", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao selecionar a Empresa!", ex.Message);
            }
        }

        [HttpPost]
        [Route("v1/empresa")]
        public async Task<ResultViewModel> Post([FromBody]EmpresaViewModel model)
        {
            try
            {
                model.Validate();
                if (model.Notifications.Count > 0)
                    return new ResultViewModel(false, "Error ao Cadastrar a Empresa", model.Notifications);
               
                var empresa = new Empresa(model.Descricao);
                if (empresa.Notifications.Count > 0)
                    return new ResultViewModel(false, "Error ao Cadastrar a Empresa", empresa.Notifications);

                var response = await _empresaRepository.AddAsync(empresa);
                return new ResultViewModel(true, "Empresa Cadastada com sucesso!", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao cadastrar a Empresa!", ex.Message);
            }
        }

        [HttpPut]
        [Route("v1/empresa")]
        public async Task<ResultViewModel> Put([FromBody]EmpresaViewModel empresa)
        {
            try
            {
                empresa.Validate();
                if (empresa.Notifications.Count > 0)
                    return new ResultViewModel(false, "Error ao Editar a Empresa", empresa.Notifications);

                var response = await _empresaRepository.UpdateAsync((Empresa)empresa);
                return new ResultViewModel(true, "Empresa Editada com sucesso!", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao editar a Empresa!", ex.Message);
            }
        }

        [HttpDelete]
        [Route("v1/empresa")]
        public async Task<ResultViewModel> Delete([FromBody]EmpresaViewModel empresa)
        {
            try
            {
                empresa.Validate();
                if (empresa.Notifications.Count > 0)
                    return new ResultViewModel(false, "Error ao Deletar a Empresa", empresa.Notifications);

                var response = await _empresaRepository.DeleteAsync((Empresa)empresa);
                return new ResultViewModel(true, "Empresa Excluida com sucesso!", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao excluir a Empresa!", ex.Message);
            }
        }
    }
}