﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using projetoempresas.Api.ViewModel;
using Domain.Entities;
using Domain.Interfaces.Repositories;
using Domain.ViewModel;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace projetoempresas.Api.Controllers
{
    [AllowAnonymous]
    public class UsuarioController : Controller
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioController(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        [HttpGet]
        [Route("v1/usuario")]
        public async Task<ResultViewModel> GetAll()
        {
            try
            {
                var response = await _usuarioRepository.RetornarTodosUsuarioOrdenadosNomeAsync();
                return new ResultViewModel(true, "ok", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao selecionar o Usuário!", ex.Message);
            }
        }

        [HttpGet]
        [Route("v1/usuario/{email}/{senha}")]
        public async Task<ResultViewModel> GetUsuario(string email, string senha)
        {
            try
            {
                var response = await _usuarioRepository.AutenticarUsuario(email, senha);
                return new ResultViewModel(true, "ok", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao selecionar o Usuário!", ex.Message);
            }
        }

        [HttpPost]
        [Route("v1/usuario")]
        public async Task<ResultViewModel> Post([FromBody]UsuarioViewModel model)
        {
            try
            {
                var email = await _usuarioRepository.ExisteEmail(model.Email);
                if (email != null)
                    return new ResultViewModel(false, "Este E-mail já está em uso!", email.Email);

                model.Validate();
                if (model.Notifications.Count > 0)
                    return new ResultViewModel(false, "Error ao Cadastrar o Usuário", model.Notifications);

                var usuario = new Usuario(model.Nome, model.SobreNome, model.Senha, model.ConfirmarSenha, model.Email);

                var response = await _usuarioRepository.AddAsync(usuario);

                if (usuario.Notifications.Count > 0)
                    return new ResultViewModel(false, "Error ao Cadastrar o Usuário", usuario.Notifications);

                return new ResultViewModel(true, "Usuário Cadastado com sucesso!", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao cadastrar o Usuário!", ex.Message);
            }
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("v1/usuario/{id}")]
        public async Task<ResultViewModel> GetById(Guid id)
        {
            try
            {
                var response = await _usuarioRepository.GetId(id);
                return new ResultViewModel(true, "ok", response);
            }
            catch (Exception ex)
            {
                return new ResultViewModel(false, "Error ao selecionar o Usuário!", ex.Message);
            }
        }
    }
}