﻿namespace projetoempresas.Api.Security
{
    public class TokenJwtOptions
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
}