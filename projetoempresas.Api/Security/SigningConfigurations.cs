﻿using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace projetoempresas.Api.Security
{
    public class SigningConfigurations

    {
        private const string SECRET_KEY = "7DEDA764-7708-4642-AE74-5FB5EB411219";

       public SigningCredentials SigningCredentials { get; }
        private readonly SymmetricSecurityKey _securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SECRET_KEY));

        public SigningConfigurations()
        {
            SigningCredentials = new SigningCredentials(_securityKey, SecurityAlgorithms.HmacSha256);
        }
    }
}